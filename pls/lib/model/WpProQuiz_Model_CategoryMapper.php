<?php
class WpProQuiz_Model_CategoryMapper extends WpProQuiz_Model_Mapper {
	
	public function fetchAll($type = WpProQuiz_Model_Category::CATEGORY_TYPE_QUESTION) {
		$type = $type == WpProQuiz_Model_Category::CATEGORY_TYPE_QUESTION ? $type : WpProQuiz_Model_Category::CATEGORY_TYPE_QUIZ;
		
		$r = array();
		
		$results = $this->_wpdb->get_results("SELECT * FROM {$this->_tableCategory} WHERE type = '". $type ."'", ARRAY_A);
		
		foreach ($results as $row) {
			$r[] =  new WpProQuiz_Model_Category($row);
		}
		//print_r($r);
		//echo "<br>";
		return $r;
	}
	
	public function fetchByQuiz($quizId) {
		$r = array();
		
		$results = $this->_wpdb->get_results($this->_wpdb->prepare('
			SELECT 
				c.*
			FROM
				'.$this->_tableCategory.' AS c
				RIGHT JOIN '.$this->_tableQuestion.' AS q
			        	ON (c.category_id = q.category_id AND c.type = %s)
			WHERE
				q.quiz_id = %d 
			GROUP BY
		          q.category_id
			ORDER BY
       			c.cat_order
		', WpProQuiz_Model_Category::CATEGORY_TYPE_QUESTION, $quizId), ARRAY_A);
		
		foreach($results as $row) {
			$r[] = new WpProQuiz_Model_Category($row);
		}
		
		return $r;
	}
	
	public function save(WpProQuiz_Model_Category $category) {
		
		$data = array('category_name' => $category->getCategoryName());
		$format = array('%s');
		$type = $category->getType();
		
		if($category->getCategoryId() == 0) {
			$this->_wpdb->insert($this->_tableCategory, array(
					'category_name' => $category->getCategoryName(),
					'category_image_name' => substr($category->getCategoryImageName(),13),
					'type' => empty($type) ? 'QUESTION' : $type
			), array('%s', '%s'));
			$category->setCategoryId($this->_wpdb->insert_id);
		} else { //echo "shyam "; print_r($_POST); die('rahul');
			/*mysql_query('update 
						`wp_wp_pro_quiz_category` set 
						`category_name`	="'.$category->getCategoryName().'",
						`cat_color`		="'.isset($_POST['categoryColor']) ? $_POST['categoryColor'] : "".'",
						`cat_order`		="'.isset($_POST['categoryOrder']) ? $_POST['categoryOrder'] : "".'",
						`top_text`		="'.isset($_POST['catTopText']) ? $_POST['catTopText'] : "".'",
						`bottom_text`	="'.isset($_POST['catBottomText']) ? $_POST['catBottomText'] : "".'",
						`category_image_name`="'.substr($category->getCategoryImageName(),13).'"
						 where `category_id`= "'.$category->getCategoryId().'"');*/
						 
						 if($category->getCategoryImageName()){
							 // echo "<pre>"; print_r($_POST['data']); die;
							 $imgName = $category->getCategoryImageName();
						 }else{
							 //echo "<pre>"; print_r($_POST['data']); die;
							  $imgName = isset($_POST['data']['cathidden_img']) ? $_POST['data']['cathidden_img'] : "default.png";
						 }
			$this->_wpdb->update(
				$this->_tableCategory, 
				array('category_name' => $category->getCategoryName(),
					  //'category_image_name' => $imgName,	
					  'category_image_name' => isset($_POST['data']['cathidden_img']) ? $_POST['data']['cathidden_img'] : "",
					  'cat_color'			=> isset($_POST['data']['categoryColor']) ? $_POST['data']['categoryColor'] : "",
					  'cat_order'			=> isset($_POST['data']['categoryOrder']) ? $_POST['data']['categoryOrder'] : "",
					  'top_text'			=> isset($_POST['data']['catTopText']) ? $_POST['data']['catTopText'] : "",
					  'bottom_text'			=> isset($_POST['data']['catBottomText']) ? $_POST['data']['catBottomText'] : ""
					  ),
				array('category_id' => $category->getCategoryId())
				);
			echo $this->_wpdb->last_query;
		}
		
		return $category;
	}
	
	public function delete($categoryId) {
		$this->_wpdb->update($this->_tableQuestion, array('category_id' => 0), array('category_id' => $categoryId), array('%d'), array('%d'));
		$this->_wpdb->update($this->_tableMaster, array('category_id' => 0), array('category_id' => $categoryId), array('%d'), array('%d'));
		
		return $this->_wpdb->delete($this->_tableCategory, array('category_id' => $categoryId), array('%d'));
	}
	
	public function getCategoryArrayForImport($type = WpProQuiz_Model_Category::CATEGORY_TYPE_QUESTION) {
		$type = $type == WpProQuiz_Model_Category::CATEGORY_TYPE_QUESTION ? $type : WpProQuiz_Model_Category::CATEGORY_TYPE_QUIZ;
		
		$r = array();
		
		$results = $this->_wpdb->get_results("SELECT * FROM {$this->_tableCategory} WHERE type = '".$type."'", ARRAY_A);
		
		foreach ($results as $row) {
			$r[strtolower($row['category_name'])] = (int)$row['category_id'];
		}
		
		return $r;
	}
}