<?php
class WpProQuiz_Controller_StyleManager extends WpProQuiz_Controller_Controller {
	
	public function route() {
		$this->show();
	}
	
	private function show() {
		
		wp_enqueue_style(
			'wpProQuiz_front_style', 
			plugins_url('css/wpProQuiz_front.min.css', WPPROQUIZ_FILE),
			plugins_url('css/pls_bootstrap.css', WPPROQUIZ_FILE),//by custome css add Amin Khan
			plugins_url('css/pls_bootstrap.min.css', WPPROQUIZ_FILE),
			plugins_url('css/pls_bootstrap-theme.min.css', WPPROQUIZ_FILE),
			plugins_url('css/pls_style.css', WPPROQUIZ_FILE),
			plugins_url('css/pls_bootstrap-theme.css', WPPROQUIZ_FILE),
			array(),
			WPPROQUIZ_VERSION
		);
		
		$view = new WpProQuiz_View_StyleManager();
		
		$view->show();
	}
}