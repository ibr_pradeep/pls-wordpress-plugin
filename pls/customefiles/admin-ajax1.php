<?php


/**
 * WordPress AJAX Process Execution.
 *
 * @package WordPress
 * @subpackage Administration
 *
 * @link http://codex.wordpress.org/AJAX_in_Plugins
 */

/**
 * Executing AJAX process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

/** Load WordPress Bootstrap */
require_once('../../../../wp-load.php' );
global $post, $wpdb;
$tableName = $wpdb->prefix.'wp_pro_quiz_question';

//print_r($_POST);
$points = array();
$TextA = array();
$TextB = array();
$sql = 'select answer_data from '.$tableName.' where id="'.$_POST['question_id'].'" and category_id="'.$_POST['category_id'].'"';
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$i=1;
if(!empty($row)){
	foreach($row as $key => $value){
		if($i<=1)
		{
			$unserialized_data = (unserialize($value));
				foreach($unserialized_data as $unserialized_data_key => $unserialized_data_val)
				{
					if($unserialized_data_key == 3)
					{
					$unserialized_data_val = (array)$unserialized_data_val;
						foreach($unserialized_data_val as $unserialized_data_val_key => $unserialized_data_val_val)
						{
						//echo $unserialized_data_val_key;
							if(substr($unserialized_data_val_key,3) == '_points'){
							$points[] = $unserialized_data_val_val;
							}
							if(substr($unserialized_data_val_key,3) == '_text_a2'){
							$TextA[] = $unserialized_data_val_val;
							}
							if(substr($unserialized_data_val_key,3) == '_text_b2'){
							$TextB[] = $unserialized_data_val_val;
							}
						}
					}
				}
			$i++;
		}
	}
}
$final = array_merge($points,$TextA,$TextB);
echo json_encode($final);


