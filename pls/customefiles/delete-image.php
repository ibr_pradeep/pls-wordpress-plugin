<?php


/**
 * WordPress AJAX Process Execution.
 *
 * @package WordPress
 * @subpackage Administration
 *
 * @link http://codex.wordpress.org/AJAX_in_Plugins
 */

/**
 * Executing AJAX process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

/** Load WordPress Bootstrap */
require_once( dirname( dirname( __FILE__ ) ) . '/wp-load.php' );
global $post, $wpdb;
$tableName = $wpdb->prefix.'wp_pro_quiz_category';

if($_POST['category_id'])
{
mysql_query('update '.$tableName.' set `category_image_name`="" where `category_id`= "'.$_POST['category_id'].'"');
}
